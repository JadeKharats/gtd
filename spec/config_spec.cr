require "./spec_helper"

describe Config do
  describe "#TODO_FILE_PATH" do
    it "return a path by concat current dir and todo_file_name" do
      Config::TODO_FILE_PATH.should eq File.join(Dir.current, "todo.txt")
    end
  end

  describe "#environment" do
    it "return development as default environment" do
      ENV.delete("GTD_ENVIRONMENT")
      Config.environment.should eq "development"
    end
    it "return passed value as environment" do
      ENV["GTD_ENVIRONMENT"]= "test"
      Config.environment.should eq "test"
    end
  end

  describe "#PORT" do
    it "return default port" do
      Config::PORT.should eq 3000
    end
  end

  describe "#BIND_IP" do
    it "return default binding" do
      Config::BIND_IP.should eq "0.0.0.0"
    end
  end
end

