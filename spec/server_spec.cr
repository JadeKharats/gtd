require "./spec_helper"

describe Server do
  describe "#new" do
    it "set default ip at 0.0.0.0" do
      Server.new.bind_ip.should eq "0.0.0.0"
    end
    it "set default port at 3000" do
      Server.new.port.should eq 3000
    end
    it "set bind_ip with argument value" do
      Server.new(bind_ip: "254.43.35.34").bind_ip.should eq "254.43.35.34"
    end
    it "set port with argument value" do
      Server.new(port: 4567).port.should eq 4567
    end
    it "set ip and port with argument value" do
      server = Server.new(bind_ip: "123.123.123.123", port: 9876)
      server.bind_ip.should eq "123.123.123.123"
      server.port.should eq 9876
    end
  end
end

