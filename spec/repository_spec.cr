require "./spec_helper"

describe Repository do
  describe "#new" do
    it "initialize Repository with given path" do
      path = File.join(Dir.current,"spec", "fixtures", "repo", "new.txt")
      repo = Repository.new(path: path)
      repo.path.should eq path
      File.exists?(path).should eq true
      File.delete(path)
    end
  end
  describe "#all" do
    it "return a list for a given file" do
      path = File.join(Dir.current,"spec", "fixtures", "repo", "all.txt")
      repo = Repository.new(path)
      repo.all.should eq ["test","all","method"]
    end
  end
  describe "#add" do
    it "store todo into the given file" do
      path = File.join(Dir.current, "spec", "fixtures", "repo", "add.txt")
      repo = Repository.new(path)
      repo.add("test add method")
      repo.all.should eq ["test add method"]
      File.delete(path)
    end
  end
end
