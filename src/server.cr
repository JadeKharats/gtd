class Server
  getter bind_ip : String
  getter port : Int32

  def initialize(@bind_ip = "0.0.0.0", @port = 3000)
    @server = HTTP::Server.new() do |context|
      request = context.request

      case request.path
      when "/"
        items = Repository.new(Config::TODO_FILE_PATH).all
        context.response.content_type = "text/html"
        context.response.print(ECR.render "src/template.ecr")
      when "/add"
        if request.method == "POST"
          body = request.body
          if body.nil?
            context.response.status_code = 400
            context.response.print("Body is required")
          else
            params = body.gets_to_end
            uri_params = URI::Params.parse(params)
            item = uri_params.fetch("item",nil)
            if item.nil?
              context.response.status_code = 400
              context.response.print("Body is required")
            else
              Repository.new(Config::TODO_FILE_PATH).add(item)
            end
            context.response.status_code = 302
            context.response.headers["Location"] = "/"
          end
        else
          context.response.status_code = 405
          context.response.headers["Allow"] = "POST"
          context.response.print("Method not allowed")
        end
      else
        context.response.status_code = 404
        context.response.print("Not found")
      end
    end
  end

  def run
    @server.bind_tcp @bind_ip, @port
    @server.listen
  end
end

