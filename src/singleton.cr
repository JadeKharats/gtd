module Singleton
  @@instance : self?

  macro included
    def self.instance
      @@instance ||= new
    end
  end
end
