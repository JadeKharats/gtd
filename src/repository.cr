class Repository
  include Singleton
  getter path : String

  def initialize(@path)
    unless File.exists?(@path)
      file = File.new(@path,"wb")
      file.close
    end
  end

  def all
    File.read_lines(@path)
  end

  def add(element : String)
    file = File.open(@path, "a")
    file.puts element
    file.close
  end
end


