module Config
  TODO_FILE_PATH = ENV.fetch("TODO_FILE_PATH", File.join(Dir.current, "todo.txt"))
  PORT = ENV.fetch("PORT", "3000").to_i
  BIND_IP = ENV.fetch("BIND_IP", "0.0.0.0")

  def self.environment
    ENV.fetch("GTD_ENVIRONMENT", "development")
  end
end

