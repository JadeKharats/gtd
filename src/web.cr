require "http/server"
require "./singleton"
require "./config"
require "./repository"
require "./server"

server = Server.new(Config::BIND_IP, Config::PORT)
if Config.environment != "test"
  server.run
end
